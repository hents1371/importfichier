<?php


namespace App\Manager;

use App\Entity\Clients;
use App\Entity\Fichier;
use App\Repository\ClientsRepository;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\String\Slugger\SluggerInterface;

class FichierManager
{
    private $targetDirectory;
    private SluggerInterface $slugger;
    private ClientsRepository $clientsRepository;

    public function __construct(SluggerInterface $slugger, $targetDirectory, ClientsRepository $clientsRepository)
    {
        $this->slugger = $slugger;
        $this->targetDirectory = $targetDirectory;
        $this->clientsRepository = $clientsRepository;
    }
    public function uploadFile(FormInterface $form)
    {
        $file = $form->get('file')->getData();
        if ($file) {
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename =  $this->slugger->slug($originalFilename);
            $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

            // ouverture du fichier Excel
            $spreadsheet = IOFactory::load($file);
            $worksheet = $spreadsheet->getActiveSheet();

            // lecture des données
            $data = [];
            foreach ($worksheet->getRowIterator() as $row) {
                $rowData = [];
                foreach ($row->getCellIterator() as $cell) {
                    $rowData[] = $cell->getValue();
                }
                $data[] = $rowData;
            }

            try {
                for($i = 1; $i < count($data); $i++) {
                $client = new Clients();
                    $client->setCompteAffaire($data[$i][0])
                        ->setCompteEvenement($data[$i][1])
                        ->setCompteDernierEvenement($data[$i][2])
                        ->setNumeroDeFiche($data[$i][3])
                        ->setLibelleCivilite($data[$i][4])
                        ->setProprietaireActuelDuVehicule($data[$i][5])
                        ->setNom($data[$i][6])
                        ->setPrenom($data[$i][7])
                        ->setNumEtNomDeLaVoie($data[$i][8])
                        ->setComplementAdressUn($data[$i][9])
                        ->setCodePostal($data[$i][10])
                        ->setVille($data[$i][11])
                        ->setTelephoneDomicile($data[$i][12])
                        ->setTelephonePortable($data[$i][13])
                        ->setTelephoneJob($data[$i][14])
                        ->setEmail($data[$i][15])
                        ->setDateMiseEnCirculationAt($this->getDateFromExcel($data[$i][16]))
                        ->setDateAchatAt($this->getDateFromExcel($data[$i][17]))
                        ->setDateDernierEvenementAt($this->getDateFromExcel($data[$i][18]))
                        ->setLibelleMarque($data[$i][19])
                        ->setLibelleModele($data[$i][20])
                        ->setVersion($data[$i][21])
                        ->setVin($data[$i][22])
                        ->setImmatriculation($data[$i][23])
                        ->setTypeDeProspect($data[$i][24])
                        ->setKilometrage($data[$i][25])
                        ->setLibelleEnergie($data[$i][26])
                        ->setVendeurVN($data[$i][27])
                        ->setVendeurVO($data[$i][28])
                        ->setCommentaireDeFacturation($data[$i][29])
                        ->setTypeVNVO($data[$i][30])
                        ->setNumeroDeDossierVNVO($data[$i][31])
                        ->setIntermediaireDeVenteVN($data[$i][32])
                        ->setDateEvenementAt($this->getDateFromExcel($data[$i][33]))
                        ->setOrigineEvenement($data[$i][34]);
                    $this->clientsRepository->save($client);
                }
            } catch (\Exception $e) {
                throw new \Exception("Erreur lors de l'enregistrement des données");
            }
            $name = $safeFilename;
                try {
                    /**
                     * @var Fichier $fichier
                     */
                    $fichier = $form->getData();
                    $fichier->setDesignation($name);
                    $file->move(
                        $this->getTargetDirectory(),
                        $fileName
                    );

                    return $fichier;
                } catch (FileException $e) {
                    throw new \Exception("Erreur lors de l'upload du fichier, veuillez réessayer");
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

        }
    }

    private function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    private function getDateFromExcel($data = [])
    {
        $date = date("Y-m-d",($data - 25569)*86400);
        return new \DateTimeImmutable($date);
    }
}