<?php

namespace App\Entity;

use App\Repository\ClientsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ClientsRepository::class)]
class Clients
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $compteAffaire = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $compteEvenement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $compteDernierEvenement = null;

    #[ORM\Column(nullable: true)]
    private ?int $numeroDeFiche = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $libelleCivilite = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $proprietaireActuelDuVehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nom = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $prenom = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $numEtNomDeLaVoie = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $complementAdressUn = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $codePostal = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $ville = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $telephoneDomicile = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $telephonePortable = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $telephoneJob = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $dateMiseEnCirculationAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $dateAchatAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $dateDernierEvenementAt = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $libelleMarque = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $libelleModele = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $version = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $vin = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $immatriculation = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $typeDeProspect = null;

    #[ORM\Column(nullable: true)]
    private ?int $kilometrage = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $libelleEnergie = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $vendeur = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $vendeurVN = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $vendeurVO = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $commentaireDeFacturation = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $typeVNVO = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $numeroDeDossierVNVO = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $intermediaireDeVenteVN = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $dateEvenementAt = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $origineEvenement = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompteAffaire(): ?string
    {
        return $this->compteAffaire;
    }

    public function setCompteAffaire(string $compteAffaire): self
    {
        $this->compteAffaire = $compteAffaire;

        return $this;
    }

    public function getCompteEvenement(): ?string
    {
        return $this->compteEvenement;
    }

    public function setCompteEvenement(?string $compteEvenement): self
    {
        $this->compteEvenement = $compteEvenement;

        return $this;
    }

    public function getCompteDernierEvenement(): ?string
    {
        return $this->compteDernierEvenement;
    }

    public function setCompteDernierEvenement(?string $compteDernierEvenement): self
    {
        $this->compteDernierEvenement = $compteDernierEvenement;

        return $this;
    }

    public function getNumeroDeFiche(): ?int
    {
        return $this->numeroDeFiche;
    }

    public function setNumeroDeFiche(?int $numeroDeFiche): self
    {
        $this->numeroDeFiche = $numeroDeFiche;

        return $this;
    }

    public function getLibelleCivilite(): ?string
    {
        return $this->libelleCivilite;
    }

    public function setLibelleCivilite(?string $libelleCivilite): self
    {
        $this->libelleCivilite = $libelleCivilite;

        return $this;
    }

    public function getProprietaireActuelDuVehicule(): ?string
    {
        return $this->proprietaireActuelDuVehicule;
    }

    public function setProprietaireActuelDuVehicule(?string $proprietaireActuelDuVehicule): self
    {
        $this->proprietaireActuelDuVehicule = $proprietaireActuelDuVehicule;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNumEtNomDeLaVoie(): ?string
    {
        return $this->numEtNomDeLaVoie;
    }

    public function setNumEtNomDeLaVoie(?string $numEtNomDeLaVoie): self
    {
        $this->numEtNomDeLaVoie = $numEtNomDeLaVoie;

        return $this;
    }

    public function getComplementAdressUn(): ?string
    {
        return $this->complementAdressUn;
    }

    public function setComplementAdressUn(?string $complementAdressUn): self
    {
        $this->complementAdressUn = $complementAdressUn;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getTelephoneDomicile(): ?string
    {
        return $this->telephoneDomicile;
    }

    public function setTelephoneDomicile(?string $telephoneDomicile): self
    {
        $this->telephoneDomicile = $telephoneDomicile;

        return $this;
    }

    public function getTelephonePortable(): ?string
    {
        return $this->telephonePortable;
    }

    public function setTelephonePortable(?string $telephonePortable): self
    {
        $this->telephonePortable = $telephonePortable;

        return $this;
    }

    public function getTelephoneJob(): ?string
    {
        return $this->telephoneJob;
    }

    public function setTelephoneJob(?string $telephoneJob): self
    {
        $this->telephoneJob = $telephoneJob;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDateMiseEnCirculationAt(): ?\DateTimeImmutable
    {
        return $this->dateMiseEnCirculationAt;
    }

    public function setDateMiseEnCirculationAt(?\DateTimeImmutable $dateMiseEnCirculationAt): self
    {
        $this->dateMiseEnCirculationAt = $dateMiseEnCirculationAt;

        return $this;
    }

    public function getDateAchatAt(): ?\DateTimeImmutable
    {
        return $this->dateAchatAt;
    }

    public function setDateAchatAt(?\DateTimeImmutable $dateAchatAt): self
    {
        $this->dateAchatAt = $dateAchatAt;

        return $this;
    }

    public function getDateDernierEvenementAt(): ?\DateTimeImmutable
    {
        return $this->dateDernierEvenementAt;
    }

    public function setDateDernierEvenementAt(?\DateTimeImmutable $dateDernierEvenementAt): self
    {
        $this->dateDernierEvenementAt = $dateDernierEvenementAt;

        return $this;
    }

    public function getLibelleMarque(): ?string
    {
        return $this->libelleMarque;
    }

    public function setLibelleMarque(?string $libelleMarque): self
    {
        $this->libelleMarque = $libelleMarque;

        return $this;
    }

    public function getLibelleModele(): ?string
    {
        return $this->libelleModele;
    }

    public function setLibelleModele(?string $libelleModele): self
    {
        $this->libelleModele = $libelleModele;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(?string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getImmatriculation(): ?string
    {
        return $this->immatriculation;
    }

    public function setImmatriculation(?string $immatriculation): self
    {
        $this->immatriculation = $immatriculation;

        return $this;
    }

    public function getTypeDeProspect(): ?string
    {
        return $this->typeDeProspect;
    }

    public function setTypeDeProspect(?string $typeDeProspect): self
    {
        $this->typeDeProspect = $typeDeProspect;

        return $this;
    }

    public function getKilometrage(): ?int
    {
        return $this->kilometrage;
    }

    public function setKilometrage(?int $kilometrage): self
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    public function getLibelleEnergie(): ?string
    {
        return $this->libelleEnergie;
    }

    public function setLibelleEnergie(?string $libelleEnergie): self
    {
        $this->libelleEnergie = $libelleEnergie;

        return $this;
    }

    public function getVendeur(): ?string
    {
        return $this->vendeur;
    }

    public function setVendeur(?string $vendeur): self
    {
        $this->vendeur = $vendeur;

        return $this;
    }

    public function getVendeurVN(): ?string
    {
        return $this->vendeurVN;
    }

    public function setVendeurVN(?string $vendeurVN): self
    {
        $this->vendeurVN = $vendeurVN;

        return $this;
    }

    public function getVendeurVO(): ?string
    {
        return $this->vendeurVO;
    }

    public function setVendeurVO(?string $vendeurVO): self
    {
        $this->vendeurVO = $vendeurVO;

        return $this;
    }

    public function getCommentaireDeFacturation(): ?string
    {
        return $this->commentaireDeFacturation;
    }

    public function setCommentaireDeFacturation(?string $commentaireDeFacturation): self
    {
        $this->commentaireDeFacturation = $commentaireDeFacturation;

        return $this;
    }

    public function getTypeVNVO(): ?string
    {
        return $this->typeVNVO;
    }

    public function setTypeVNVO(?string $typeVNVO): self
    {
        $this->typeVNVO = $typeVNVO;

        return $this;
    }

    public function getNumeroDeDossierVNVO(): ?string
    {
        return $this->numeroDeDossierVNVO;
    }

    public function setNumeroDeDossierVNVO(?string $numeroDeDossierVNVO): self
    {
        $this->numeroDeDossierVNVO = $numeroDeDossierVNVO;

        return $this;
    }

    public function getIntermediaireDeVenteVN(): ?string
    {
        return $this->intermediaireDeVenteVN;
    }

    public function setIntermediaireDeVenteVN(?string $intermediaireDeVenteVN): self
    {
        $this->intermediaireDeVenteVN = $intermediaireDeVenteVN;

        return $this;
    }

    public function getDateEvenementAt(): ?\DateTimeImmutable
    {
        return $this->dateEvenementAt;
    }

    public function setDateEvenementAt(?\DateTimeImmutable $dateEvenementAt): self
    {
        $this->dateEvenementAt = $dateEvenementAt;

        return $this;
    }

    public function getOrigineEvenement(): ?string
    {
        return $this->origineEvenement;
    }

    public function setOrigineEvenement(?string $origineEvenement): self
    {
        $this->origineEvenement = $origineEvenement;

        return $this;
    }
}
