<?php

namespace App\Controller;

use App\Entity\Fichier;
use App\Form\FichierType;
use App\Manager\FichierManager;
use App\Repository\FichierRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FichierController extends AbstractController
{

    private FichierManager $fichierManager;
    public function __construct(FichierManager $fichierManager){
        $this->fichierManager = $fichierManager;
    }

    #[Route('/', name: 'app_fichier', methods: ['GET', 'POST'])]
    public function index(Request $request, FichierRepository $fichierRepository): Response
    {
        $fichier = new Fichier();
        $form = $this->createForm(FichierType::class, $fichier);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $this->fichierManager->uploadFile($form);
                $this->addFlash('success', 'Les données sont enregistrées dans la base de donnée avec succès.');
                $fichierRepository->save($fichier, true);
            }catch (\Throwable $throwable){
                $this->addFlash('error', "Erreur lors de l'import du fichier");
            }
        }

        return $this->render('fichier/index.html.twig', [
            'formImport' => $form->createView(),
        ]);
    }
}
